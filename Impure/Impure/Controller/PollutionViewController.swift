//
//  ViewController.swift
//  Impure
//
//  Created by Robert Szost on 10.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class PollutionViewController: UIViewController, CLLocationManagerDelegate {

    //Get data from API
    let STATIONLIST_URL = "http://api.gios.gov.pl/pjp-api/rest/station/findAll"
    let STATIONMEASURE_URL = "http://api.gios.gov.pl/pjp-api/rest/station/sensors/"
    let STATUS_URL = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/"
    let VALUE_URL = "http://api.gios.gov.pl/pjp-api/rest/data/getData/"

    //Variables
    let locationManager = CLLocationManager()
    var stationList = [Station]()
    var userCordinates = [Double]()

    //IBOutlets
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var pollutionIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }


    //Get all stations JSON
    func getAllStationsJSON(url: String) {

        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                let stationsJSON: JSON = JSON(response.result.value!)
                self.parseJSONToArray(stationsJSON: stationsJSON)
                self.getClosestStation(userCordinates: self.userCordinates, stationToCalculateList: self.stationList)
            } else {
                self.cityLabel.text = "Connection Issues"
            }
        }
    }


    //Grab user location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil

            getAllStationsJSON(url: STATIONLIST_URL)
            //getMeasurementStationsJSON(url: STATIONMEASURE_URL, stationId: stationList[0].id)

            print("Longitude = \(location.coordinate.longitude), Latitude = \(location.coordinate.latitude)")

            let latitude = Double(location.coordinate.latitude)
            let longitude = Double(location.coordinate.longitude)

            userCordinates = [latitude, longitude]
        }
    }

    //Shows error if gps not working
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        cityLabel.text = "Location Unavailable"
    }

    //Taking only some parts of JSON + putting them into array
    func parseJSONToArray(stationsJSON: JSON) {
        for loop in 0...(stationsJSON.count - 1) {
            let station = Station(id: (stationsJSON[loop]["id"].intValue), gegrLat: (stationsJSON[loop]["gegrLat"].doubleValue), gegrLon: (stationsJSON[loop]["gegrLon"].doubleValue), distance: 999, stationName: (stationsJSON[loop]["stationName"].stringValue))
            stationList.append(station)
        }
    }

    //Function to get closest station from user
    func getClosestStation(userCordinates: [Double], stationToCalculateList: [Station]) {
        for loop in 0...(stationList.count-1) {
            let distance = sqrt(pow((userCordinates[0] - stationList[loop].gegrLat), 2) + pow((userCordinates[1] - stationList[loop].gegrLon), 2))
            stationList[loop].distance = distance

    //Sorting from closest distance
            stationList.sort {
                $0.distance < $1.distance
            }
        }
        updateViews(stationList: stationList)
    }
    
    //This function should pick correct measurment station and then use id of measurment station to get data of air quality from other link STATUS_URL = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/", API was dead so I cannot finish this ;/
    func getMeasurementStationsJSON (url: String, stationId: Int){

        var urlWithStation = "\(url)\(stationId)"
        
            Alamofire.request(urlWithStation, method: .get).responseJSON {
                response in
                if response.result.isSuccess {

                } else {

                }
            }
        }
    
    //Update screen
    func updateViews(stationList: [Station]) {
        cityLabel.text = stationList[0].stationName
    }
}

