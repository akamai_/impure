//
//  AirPolutionDataModel.swift
//  Impure
//
//  Created by Robert Szost on 14.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation

class AirPollutionDataModel {
    
    //Properties of model
    var city: String = "Wroclaw"
    var imageName: String = ""
    var polutionConditon: String = "Dobre"
    var polutionMeter: Double = 30.30


    
    //Initialisation
    init(city: String, imageName: String, polutionConditon: String, polutionMeter: Double) {
        self.city = city
        self.imageName = imageName
        self.polutionConditon = polutionConditon
        self.polutionMeter = polutionMeter
    }
    
}
