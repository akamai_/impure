//
//  Station.swift
//  Impure
//
//  Created by Robert Szost on 14.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation

struct Station {

    //Properties of model
    var id: Int!
    var gegrLat: Double!
    var gegrLon: Double!
    var distance: Double!
    var stationName: String!
    
    //Initialisation
    init(id: Int, gegrLat: Double, gegrLon: Double, distance: Double, stationName: String) {
        self.id = id
        self.gegrLat = gegrLat
        self.gegrLon = gegrLon
        self.distance = distance
        self.stationName = stationName
    }
}
